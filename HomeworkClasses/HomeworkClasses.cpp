// HomeworkClasses.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

class matrix
{
private:
	double matr[10][10] = { {0} };
	int rows;
	int columns;
public:
	matrix();
	~matrix();

	bool input();
	bool print();
	bool summMatrix(matrix matr1);
	bool multMatrix(matrix matr1);
	bool transp();
	int getRows()
	{
		return rows;
	}
	int getColumns()
	{
		return columns;
	}
	double getElem(int row, int col)
	{
		return matr[row][col];
		
	}
};

matrix::matrix()
{
}

matrix::~matrix()
{
}

bool matrix::input()
{
	std::cout << "vvedite  kol-wo strok";
	int check;
	std::cin >> check;
	if (check <= 10 && check >= 1)
	{
		rows = check;
	}
	else
	{
		std::cout << "Error";
		return false;
	}

	std::cout << "vvedite  kol-wo stolbikov";
	check = 0;
	std::cin >> check;
	if (check <= 10 && check >= 1)
	{
		columns = check;
	}
	else
	{
		std::cout << "Error";
		return false;
	}
	std::cout << "vvedite elementy matritcy";
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			std::cin >> matr[i][j];
		}
	}
}

bool matrix::print()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			std::cout << matr[i][j] << ' ';
		}
		std::cout << std::endl;
	}
	return true;
}

bool matrix::summMatrix(matrix matr1)
{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				matr[i][j] += matr1.matr[i][j];
			}
		}
		std::cout << std::endl << "slojenie matritc" << std::endl;
		return(0);
}

bool matrix::multMatrix(matrix matr1)
{
	if (rows == matr1.columns)
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < matr1.columns; j++)
			{
				int mid = 0;
				for (int k = 0; k < rows; k++)
				{
					mid += matr[i][k] * matr1.matr[k][j];
				}
				matr[i][j] = mid;
			}
		}
		std::cout << std::endl << "ymnojenie matritc" << std::endl;
		return true;
	}
	else
	{
		std::cout << std::endl << "Error" << std::endl;
		return false;
	}
}

bool matrix::transp()
{
	double sw = 0;
	for (int i = 0; i < columns; i++)
	{
		for (int j = i; j < rows; j++)
		{
			sw = matr[i][j];
			matr[i][j] = matr[j][i];
			matr[j][i] = sw;
		}
	}
	std::cout << std::endl << "Transportirovka" << std::endl;
	return true;
}

class vector : matrix
{
private:
	int size;
	double vec[10];
public:
	double scalMultVector(vector vec2);
	double getElem(int n);
	void vecMult(double k);
};

double vector::scalMultVector(vector vec2)
{
		double sum = 0;
		for (int i = 0; i < size; i++)
		{
			sum += vec[i] * vec2.vec[i];
		}
		return sum;
	}

double vector::getElem(int n)
{
		return vec[n];
	}
void vector::vecMult(double k)
{
	for (int i = 0; i < size; i++)
	{
		vec[i]= k;
	}
}

int main()
{
	matrix matrA, matrB;
	matrA.input();
	matrB.input();
	matrA.transp();
	matrA.print();
	matrA.summMatrix(matrB);
	matrA.print();
	matrA.multMatrix(matrB);
	matrA.print();
	return 0;
}