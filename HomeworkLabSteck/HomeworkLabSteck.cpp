// Самостоятельные задания:
#include "stdafx.h"
#include <iostream>
#include <string>
#include <queue>  
using namespace std;

//// 1) Проверка правильности введения скобок

int main()
{
	setlocale(0, "rus");
	string s;
	cout << "Введите пример для проверки:" << endl;
	getline(cin, s);
	stack<char> stk;
	char a = 0;
	stk.push(s[0]);

	for (int i = 1; i < s.size(); i++) // так как 0 уже в стеке начинаем с 1-и
	{
		if (s[i] == '(' || s[i] == '[' || s[i] == '{') // Если записывается ( || [ || { то они помещаются в stack
		{
			stk.push(s[i]);
		}
		else if (s[i] == ')')
		{
			if (stk.size() > 0 && (stk.top() == '('))
				stk.pop();
		}
		else if (s[i] == ']')
		{
			if (stk.size() > 0 && (stk.top() == '['))
				stk.pop();
		}
		else if (s[i] == '}')
		{
			if (stk.size() > 0 && (stk.top() == '{'))
				stk.pop();
		}
	}
	if (stk.empty())
	{
		cout << "<<Stack is empty>>" << endl;
		cout << "<<True>>" << endl; // выводится при правильном порядке скобок
	}
	else
	{
		cout << "<<Stack is not empty>>" << endl;
		cout << "<<False>>" << endl;// выводится при не правильном  порядке скобок
	}
}

