#include "pch.h"
#include<iostream>

using namespace std;

class matrix
{
protected:

	int m, n, i, j;
	double **mat;

public:
	matrix() {};
	matrix(unsigned int rows, unsigned int cols)
	{


		m = rows; n = cols;
		mat = new double*[m];
		for (int i = 0; i < m; i++) {
			mat[i] = new double[n];
		}
	};


	void set()// Ввод матрицы
	{
		cout << "Введите матрицу : " « endl;
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				cin » mat[i][j];
			}
		}
		cout « endl;
	}


	void get()// Вывод матрицы
	{
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				cout « " " « mat[i][j] « " ";
			}
			cout « endl;
		}
	}




	matrix operator += (const matrix &v) { // сложение унарное
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < v.n; j++) {
				this->mat[i][j] = mat[i][j] + v.mat[i][j];
			}
		}
		return(*this);
	}
	matrix operator *=(const matrix &l) { //умножение унарное
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < l.n; j++)
			{

				for (int k = 0; k < l.n; k++)
				{
					this->mat[i][j] = mat[i][k] * l.mat[k][j];
				}
			}
		}
		return(*this);

	}
	friend matrix operator + (const matrix &f, const matrix &d);
	friend matrix operator * (const matrix &x, const matrix &y);
};

matrix operator * (const matrix &x, const matrix &y)// умножение бинарное
{
	matrix o(x.m, y.n);// Держит результат
	for (int i = 0; i < x.m; i++)
	{
		for (int j = 0; j < y.n; j++)
		{
			o.mat[i][j] = 0;
			for (int k = 0; k < y.n; k++)
			{
				o.mat[i][j] += x.mat[i][k] * y.mat[k][j];
			}
		}
	}
	return(o);
}

matrix operator + (const matrix &f, const matrix &d)// сложение бинарное
{
	matrix z(f.m, d.n);// Держит результат
	for (int i = 0; i < f.m; i++)
	{
		for (int j = 0; j < d.n; j++)
		{
			z.mat[i][j] = f.mat[i][j] + d.mat[i][j];
		}
	}
	return(z);
}
int main()
{
	setlocale(LC_ALL, "Russian");
	matrix a(2, 2);
	matrix b(2, 2);
	matrix t(2, 2);
	matrix z;
	matrix o;
	a.set();
	b.set();
	t.set();
	z = a + b;
	a += b;
	o = a * b;
	t *= b;


	cout << "Произведение бинарное: " « endl;
	o.get();
	cout << endl;
	cout << "Сумма бинарная: " « endl;
	z.get();
	cout << endl;
	cout << "Произведение унарное" « endl;
	t.get();
	cout << endl;
	cout << "Сложение унарное " « endl;
	a.get();
	cout << endl;


	system("pause");
	getchar();
	return 0;


}
