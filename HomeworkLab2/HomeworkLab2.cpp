// 181_352_Goncharov.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
// Задание №1 поэлементное умножени матриц

#include "stdafx.h"
#include <iostream>
int dd()
{
	int A[3][3] = { { 1, 0, 2 },{ 2, 1, 3 },{ 3, 4, 8 } };
	int B[3][3] = { { 3, 1, 2 },{ 1, 0, 2 },{ 2, 0, 3 } };
	int C[3][3] = { { 0, 0, 0 },{ 0, 0, 0 },{ 0, 0, 0 } };

	for (int stroka = 0; stroka < 3; stroka++) {
		for (int stolbec = 0; stolbec < 3; stolbec++) {
			for (int k = 0; k < 3; k++) {
				C[stroka][stolbec] += A[stroka][k] * B[k][stolbec];
			}
			std::cout << C[stroka][stolbec] << "  ";
		}
		std::cout << "\n";
	}
	getchar();
	return(0);
}
int ss()
{
	int A[3][3] = { { 1, 0, 2 },{ 2, 1, 3 },{ 3, 4, 8 } };
	int B[3][1] = { { 3 },{ 1 },{ 2 } };
	int C[3][1] = { { 0 },{ 0 },{ 0 } };

	for (int stroka = 0; stroka < 3; stroka++) {
		for (int stolbec = 0; stolbec < 1; stolbec++) {
			for (int k = 0; k < 3; k++) {
				C[stroka][stolbec] += A[stroka][k] * B[k][stolbec];
			}
			std::cout << C[stroka][stolbec] << "  ";
		}
		std::cout << "\n";
	}
	getchar();
	return(0);
}
int ff()
{
	int A[3][1] = { { 1 },{ 2 },{ 3 } };
	int B[3][1] = { { 3 },{ 1 },{ 2 } };
	int C[3][1] = { { 0 },{ 0 },{ 0 } };

	for (int stroka = 0; stroka < 3; stroka++) {
		for (int stolbec = 0; stolbec < 1; stolbec++) {
			C[stroka][stolbec] = A[stroka][stolbec] * B[stroka][stolbec];
			std::cout << C[stroka][stolbec] << "  ";
		}
		std::cout << "\n";
	}
	getchar();
	return(0);
}
int main()
{
	int A[3][3] = { { 1, 0, 2 },{ 2, 1, 3 },{ 3, 4, 8 } };
	int B[3][3] = { { 3, 1, 2 },{ 5, 6, 4 },{ 2, 4, 4 } };
	int C[3][3] = { { 0, 0, 0 },{ 0, 0, 0 },{ 0, 0, 0 } };

	for (int stroka = 0; stroka < 3; stroka++) {
		for (int stolbec = 0; stolbec < 3; stolbec++) {
			{ C[stroka][stolbec] += A[stroka][stolbec] * B[stroka][stolbec];
			}
			std::cout << C[stroka][stolbec] << "  ";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
	dd();
	std::cout << "\n";
	ss();
	std::cout << "\n";
	ff();
}