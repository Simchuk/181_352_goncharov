//Задание№1 Реализовать шифр Цезаря для массива, заданного в тексте программы.

#include "pch.h"
#include <iostream>;
#include <locale.h>;
using namespace std;

void bubble(int arr[])
{
	int sw = 0;
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10 - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				sw = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = sw;
			}
		}

	}
	for (int i = 0; i < 10; i++)
	{
		std::cout << arr[i] << '\t';
	}
	std::cout << std::endl;
}
void shakemeup(int shake[])
{
	int sw;
	int size = 10;
	int s = 1;
	int e = size - 1;
	std::cout << std::endl;
	while (s <= e)
	{
		for (int i = e; i >= s; i--)
		{
			if (shake[i - 1] > shake[i])
			{
				sw = shake[i];
				shake[i] = shake[i - 1];
				shake[i - 1] = sw;
			}
		}
		s++;
		for (int i = s; i <= e; i++)
		{
			if (shake[i - 1] > shake[i])
			{
				sw = shake[i];
				shake[i] = shake[i - 1];
				shake[i - 1] = sw;
			}
		}
		e--;
	}
	for (int i = 0; i < 10; i++)
	{
		std::cout << shake[i] << ' ';
	}
}
int main()
{//Шифр цезаря
	char bukv[26] = { 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z' };
	int sum = 3;
	int t = 0;
	cout << "vvedite 10 bukv";
	cout << '\n';
	for (int i = 0; i < 10; i++) {
		cin >> bukv[i];
	}

	for (int i = 0; i < 10; i++) {
		t = bukv[i] + sum;
		bukv[i] = char(t);
	}
	cout << '\n';
	cout << "zashifrovanye bukvi - ";
	for (int i = 0; i < 10; i++) {
		cout << bukv[i];
	}
	cout << '\n';
	cout << '\n';
	
	//Сортироврка пузырьком
	std::cout << "Bubble sorting" << '\n';
	int bub[10];
	for (int i = 0; i < 10; i++)
	{
		bub[i] = 1 + rand() % 100;
		std::cout << bub[i] << '\t';
	}
	std::cout << '\n';
	bubble(bub);
	std::cout << '\n';
   //Сортировка шейкером
	std::cout << "Shaker sorting" << '\n';
	int shake[10];
	for (int i = 0; i < 10; i++)
	{
		shake[i] = 1 + rand() % 100;
		std::cout << shake[i] << ' ';
	}
	shakemeup(shake);
	return 0;
}