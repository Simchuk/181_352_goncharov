﻿// HomeworkDynamic.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
class dynamic_matrix
{
private:
	double matr[10][10] = { {0} };
	int rows;
	int columns;
public:
	dynamic_matrix();
	~dynamic_matrix();

	bool input();
	bool print();
	bool summMatrix(dynamic_matrix matr1);
	bool multMatrix(dynamic_matrix matr1);
	bool transp();
	int getRows()
	{
		return rows;
	}
	int getColumns()
	{
		return columns;
	}
	double getElem(int row, int col)
	{
		return matr[row][col];

	}
};
dynamic_matrix::dynamic_matrix()
{
}

dynamic_matrix::~dynamic_matrix()
{
}

int main()
